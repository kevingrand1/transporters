/// <reference types="vite/client" />

declare module '*.vue';

interface ImportMetaEnv {
  readonly VITE_API_KEY: string
  readonly VITE_API_IMAGE_KEY: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}

/*
declare module 'vue' {
  import { CompatVue } from 'vue'
  const Vue: CompatVue
  export default Vue
  export * from '@vue/runtime-dom'
  const { configureCompat } = Vue
  export { configureCompat }
}
*/


