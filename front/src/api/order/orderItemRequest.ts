import {useCookies} from "vue3-cookies";
import makeRequest from "@/api/makeRequest";
import {ApiRoute} from "@/config/ApiRoute";
import {pathReplace} from "@/util/StrUtil";
import type {IAddItemOrder} from "@/interface/Order/IAddItemOrder";
import type {IRemoveOrderItem} from "@/interface/Order/IRemoveOrderItem";
import type {IOrder} from "@/interface/Order/IOrder";
import type {IOrderItem} from "@/interface/Order/IOrderItem";

export const addItemToOrder = async (): Promise<IAddItemOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.items.addItem, {cartToken: cookies.get('cartToken')}), 'POST')
}

export const removeItemFromOrder = async (itemId: string): Promise<IRemoveOrderItem> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.items.deleteItem, {cartToken: cookies.get('cartToken'), itemId}), 'DELETE')
}

export const validateItems = async (items: IOrderItem[]): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.items.check, {
    cartToken: cookies.get('cartToken')
  }), 'POST', items)
}

export const getItemsWeightList = async (): Promise<number[]> => {
  return makeRequest(ApiRoute.order.items.weightList, 'GET')
}
