import makeRequest from "@/api/makeRequest";
import {ApiRoute} from "@/config/ApiRoute";
import type {IValidOrderAddress} from "@/interface/OrderAddress/IValidOrderAddress";
import {useCookies} from "vue3-cookies";
import type {IValueLabel} from "@/interface/IValueLabel";
import type {IOrder} from "@/interface/Order/IOrder";

export const getAddressStatusList = (): Promise<IValueLabel[]> => {
  return makeRequest(ApiRoute.orderAddress.getStatusList, 'get')
}

export const validateAddress = (data: IValidOrderAddress): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(ApiRoute.orderAddress.validateAddress.replace(':cartToken', cookies.get('cartToken')), 'post', data)
}
