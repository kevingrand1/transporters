import makeRequest from "@/api/makeRequest";
import {ApiRoute} from "@/config/ApiRoute";
import {useCookies} from "vue3-cookies";
import type {IOrder} from "@/interface/Order/IOrder";
import {pathReplace} from "@/util/StrUtil";
import type {IOrderExtra} from "@/interface/Order/IOrderExtra";

export const getOrInitiateOrder = async (): Promise<IOrder> => {
  const {cookies} = useCookies()
  return new Promise((resolve, reject) => {
    return makeRequest(ApiRoute.order.getOrInitiateOrder, 'POST', {cartToken: cookies.get('cartToken')}).then((response) => {
      cookies.set('cartToken', response.cartToken)
      resolve(response)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const getOrder = async (): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.getOrder, {
    cartToken: cookies.get('cartToken')
  }), 'GET')
}

export const validateOrderExtra = async (data: IOrderExtra): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.validateOrderExtra, {
    cartToken: cookies.get('cartToken')
  }), 'POST', data)
}

export const updatePrice = async (price: number): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.updatePrice, {
    cartToken: cookies.get('cartToken')
  }), 'POST', {price})
}

export const updateDeliveryDate = async (startDate: Date, endDate: Date): Promise<IOrder> => {
  const {cookies} = useCookies()
  return makeRequest(pathReplace(ApiRoute.order.updateDeliveryDate, {
    cartToken: cookies.get('cartToken')
  }), 'POST', {startDate, endDate})
}

export const getAvailableDelivery = async (limit: number | null = null): Promise<IOrder[]> => {
  return makeRequest(ApiRoute.order.getAvailableDelivery, 'post', {limit})
}
