import makeRequest from "./makeRequest";
import type {IRolesListRequest} from "@/interface/request/IRolesListRequest";
import {ApiRoute} from "@/config/ApiRoute";
import type {IUser} from "@/interface/IUser";

type Pagination = {
  page?: number;
  limit?: number;
  search?: string;
}

export type IPaginationResult = [data: IUser[], total: number];

export const getDeliverListRequest = async (pagination: Pagination = {page: 1, limit: 10}): Promise<IPaginationResult> => {
  return await makeRequest(ApiRoute.user.deliverList, 'POST', pagination);
}

export const getSuppliersAndConsumersRequest = async (pagination: Pagination = {page: 1, limit: 10}): Promise<IPaginationResult> => {
  return await makeRequest(ApiRoute.user.suppliersAndConsumers, 'POST', pagination);
}

export const getRolesListRequest = async (): Promise<IRolesListRequest> => {
  return await makeRequest(ApiRoute.user.rolesList, 'POST');
}

export const getUserStatsRequest = async () => {
  return await makeRequest(ApiRoute.user.userStatistics, 'POST');
}
