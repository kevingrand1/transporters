import axios from "axios";

export interface IGeocodeResponse {
  attribution: string;
  features: IGeocodeFeature[];
  filter: Record<string, string>;
  licence: string;
  limit: number;
  query: string;
  type: string;
  version: string;
}

export interface IGeocodeFeature {
  geometry: IGeocodeGeometry;
  properties: IGeocodeProperties;
  type: string;
}

export interface IGeocodeGeometry {
  coordinates: number[];
  type: string;
}

export interface IGeocodeProperties {
  city: string;
  citycode: string;
  context: string;
  housenumber: string;
  id: string;
  importance: number;
  label: string;
  name: string;
  postcode: string;
  score: number;
  street: string;
  type: string;
  x: number;
  y: number;
}

const makeGeocodeRequest = async (address: string): Promise<IGeocodeResponse> => {
  const url = `https://api-adresse.data.gouv.fr/search/?q=${encodeURIComponent(address)}&limit=7&autocomplete=1&type=housenumber`
  return new Promise((resolve, reject) => {
    axios({method: 'GET', url}).then((response) => {
      resolve(response.data)
    }).catch((error) => {
      reject(error.response.data);
    })
  })
}

export default makeGeocodeRequest
