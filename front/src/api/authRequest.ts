import makeRequest from "@/api/makeRequest";
import type {ILogin} from "@/interface/ILogin";
import type {IResponse} from "@/interface/IResponse";
import type {IRegister} from "@/interface/IRegister";
import type {IUpdatePassword} from "@/interface/IUpdatePassword";
import type {IUser} from "@/interface/IUser";
import {ApiRoute} from "@/config/ApiRoute";

export const loginRequest = async (email: string, password: string): Promise<ILogin> => {
  return await makeRequest(ApiRoute.auth.login, 'POST', {email, password}, false);
}

export const registerRequest = async (userData: IRegister): Promise<IResponse> => {
  return await makeRequest(ApiRoute.auth.register, 'POST', userData, false);
}

export const refreshTokenRequest = async (): Promise<ILogin> => {
  return await makeRequest(ApiRoute.auth.refreshToken, 'POST');
}

export const updateUserPasswordRequest = async (userData: IUpdatePassword): Promise<ILogin> => {
  return await makeRequest(ApiRoute.auth.updatePassword, 'PATCH', userData);
}

export const updateUserInfoRequest = async (userData: IUser): Promise<ILogin> => {
  return await makeRequest(ApiRoute.auth.updateInfo, 'PATCH', userData);
}
