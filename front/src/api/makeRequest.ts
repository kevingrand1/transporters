import axios from "axios";
import {API_URL} from "@/config/AppConfig";
import {useCookies} from "vue3-cookies";
import type {IUser} from "@/interface/IUser";

const makeRequest = (url: string, method: string, body?: any, auth: boolean = true): Promise<any> => {
  const {cookies} = useCookies()
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${cookies.get('token')}`
  }

  if (body as IUser && auth) delete body._id;

  return new Promise((resolve, reject) => {
    axios({
      url,
      method,
      baseURL: API_URL,
      data: body,
      headers: auth ? headers : {headers: headers['Content-Type']}
    })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response.data);
      });
  });
}


export default makeRequest;

