import NewItemView from "@/views/ride/NewItemView.vue";
import {
  RideNewDatesRoute,
  RideNewExtraRoute,
  RideNewItemsRoute,
  RideNewPriceRoute,
  RideNewTransportRoute
} from "@/config/FrontRoute";
import NewTransportRideView from "@/views/ride/NewTransportRideView.vue";
import NewItemExtraView from "@/views/ride/NewItemExtraView.vue";
import NewItemPriceView from "@/views/ride/NewItemPriceView.vue";
import NewItemDatesView from "@/views/ride/NewItemDatesView.vue";

const RideRouter = [
  {
    path: 'new/items',
    name: RideNewItemsRoute,
    component: NewItemView
  },
  {
    path: 'new/transport',
    name: RideNewTransportRoute,
    component: NewTransportRideView
  },
  {
    path: 'new/extra',
    name: RideNewExtraRoute,
    component: NewItemExtraView
  },
  {
    path: 'new/price',
    name: RideNewPriceRoute,
    component: NewItemPriceView
  },
  {
    path: 'new/dates',
    name: RideNewDatesRoute,
    component: NewItemDatesView
  },
]

export default RideRouter
