import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/auth/LoginView.vue";
import RegisterView from "@/views/auth/RegisterView.vue";
import {refreshTokenRequest} from "@/api/authRequest";
import ProfileView from "@/views/user/ProfileView.vue";
import {ROLE_ADMIN, ROLES_LIST} from "@/config/Constant";
import {compareTables} from "@/util/ArrayUtil";
import AdminRouter from "@/router/AdminRouter";
import {useCookies} from "vue3-cookies";
import {userStore} from "@/stores/userStore";
import AdsView from "@/views/AdsView.vue";
import {
  AdminRoute,
  AdsRoute,
  CartRoute,
  HomeRoute,
  InProgressRoute, LoginRoute, ProfileRoute, RegisterRoute, RideRoute, RideSearchRoute,
} from "@/config/FrontRoute";
import NotFound from "@/views/error/NotFound.vue";
import RideRouter from "@/router/RideRouter";
import RideSearchView from "@/views/ride/RideSearchView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: HomeRoute,
      component: HomeView
    },
    {
      path: '/ads',
      name: AdsRoute,
      component: AdsView
    },
    {
      path: '/admin',
      name: AdminRoute,
      children: AdminRouter,
      meta: {
        roles_access: [ROLE_ADMIN]
      }
    },
    {
      path: '/ride',
      name: RideRoute,
      children: RideRouter
    },
    {
      path: '/ride/search',
      name: RideSearchRoute,
      component: RideSearchView
    },
    {
      path: '/profile',
      name: ProfileRoute,
      component: ProfileView,
      meta: {
        roles_access: ROLES_LIST
      }
    },
    {
      path: '/connexion',
      name: LoginRoute,
      component: LoginView
    },
    {
      path: '/inscription',
      name: RegisterRoute,
      component: RegisterView
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  const {cookies} = useCookies()
  if (!userStore().authenticated && cookies.get('token')) {
    try {
      const user = await refreshTokenRequest()
      userStore().login(user)
    } catch (e) {
      userStore().logout()
    }
  }

  if (to.meta.roles_access) {
    if (!userStore().authenticated) {
      // TODO: maybe redirect to 401 page
      next({name: LoginRoute})
    } else {
      if (compareTables(to.meta.roles_access, userStore().user.roles)) {
        next()
      } else {
        next({name: InProgressRoute});
      }
    }
  }
  next()
});

export default router
