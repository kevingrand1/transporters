import AdminView from "@/views/admin/AdminView.vue";
import {DelivererHomeRoute} from "@/config/FrontRoute";

const DelivererRouter = [
  {
    path: 'deliverer',
    name: DelivererHomeRoute,
    component: AdminView
  },
]

export default DelivererRouter
