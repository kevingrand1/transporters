import AdminView from "@/views/admin/AdminView.vue";

import {
  AdminHomeRoute,
} from "@/config/FrontRoute";

const AdminRouter = [
  {
    path: '',
    name: AdminHomeRoute,
    component: AdminView
  },
]

export default AdminRouter
