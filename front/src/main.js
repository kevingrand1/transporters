import 'bootstrap'
import './assets/main.css'
import {createApp} from 'vue'
import VueCookies from 'vue3-cookies'
import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-bootstrap.css';
import {createPinia} from "pinia";
import {defaultLocale, languages} from "@/locales";

import App from './App.vue'
import router from "@/router";
import {createI18n} from "vue-i18n";

const pinia = createPinia()

const i18n = createI18n({
  locale: defaultLocale,
  fallbackLocale: defaultLocale,
  messages: Object.assign(languages)
})

const app = createApp(App)
app.use(ToastPlugin);
app.use(pinia)
app.use(router)
app.use(VueCookies, {
  expireTimes: "1d",
})
app.use(i18n)

app.mount('#app')
