import {useToast} from "vue-toast-notification";

const toast = (message: string, type: string) => {
  const $toast = useToast();
  $toast.open({
    message,
    position: 'top-right',
    type,
    duration: 2000,
    dismissible: true,
  })
}

export const successToast = (message: string) => {
  return toast(message, 'success');
}

export const errorToast = (message: string) => {
  return toast(message, 'error');
}

export const infoToast = (message: string) => {
  return toast(message, 'info');
}

export const warningToast = (message: string) => {
  return toast(message, 'warning');
}
