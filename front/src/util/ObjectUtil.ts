export const getNested = <T>(obj: T, path: string, defaultValue: unknown = 'Unknown'): string => {
  const value = path.split('.').reduce((acc: any, key: string) => (acc !== null && acc !== undefined && key in acc) ? acc[key] : undefined, obj);
  return (value !== undefined) ? value : defaultValue;
}
