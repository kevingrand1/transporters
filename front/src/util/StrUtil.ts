export const pathReplace = (path: string, params: Record<string, string>) => {
  let newPath = path;
  for (const [key, value] of Object.entries(params)) {
    newPath = newPath.replace(`:${key}`, value);
  }
  return newPath;
};
