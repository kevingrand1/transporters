
export const compareTables = (table1: string[], table2: string[]) : boolean => {
  for(let i = 0; i < table1.length; i++){
    if (table2.includes(table1[i])){
      return true
    }
  }
  return false
}