import type {ILogin} from "@/interface/ILogin";

export interface IUserStore extends ILogin {
  authenticated: boolean;
}
