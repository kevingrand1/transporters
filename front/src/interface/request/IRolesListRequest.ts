export interface IRolesListRequest {
  default: string;
  roles: [{label: string, value: string}]
}
