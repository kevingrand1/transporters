import type {IUser} from "@/interface/IUser";

export interface ILogin {
  token: string,
  user: IUser
}
