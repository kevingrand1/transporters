export interface IUser {
  firstname: string;
  password: string;
  address: string;
  salt: string;
  humanizeRole: string;
  roles: any[];
  fullName: string;
  _id: ObjectConstructor;
  email: string;
  enabled: boolean;
  lastname: string
  phone: string;
  isAdmin: boolean;
  isIndividual: boolean;
  isRetailer: boolean;
  isDeliveryman: boolean;
}
