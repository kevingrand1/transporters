import type {IAddress} from "@/interface/OrderAddress/IAddress";

export interface IOrderTransportAddress {
  address: Partial<IAddress>
  flexible: boolean;
  help: boolean;
}
