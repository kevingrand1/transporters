import type {IOrderTransportAddress} from "@/interface/OrderAddress/IOrderTransportAddress";

export interface IValidOrderAddress {
  status: string | null;
  fromAddress: Partial<IOrderTransportAddress>;
  toAddress: Partial<IOrderTransportAddress>;
}
