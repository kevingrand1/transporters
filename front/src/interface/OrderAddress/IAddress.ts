export interface IAddress {
  city: string | null
  postcode: string | null
  street: string | null
  housenumber: string | null
  fullAddress: string | null
}
