export interface IRegister {
  lastname: string
  firstname: string
  email: string
  address: string
  password: string
  confirmPassword: string
  phone: string
  roles: string[]
}
