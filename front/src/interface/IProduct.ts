export interface IProduct {
  name: string
  description?: string
  size?: string
  travel?: string
  price?: string
  picture: string
}
