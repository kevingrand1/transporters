export interface IOrderExtra {
  extraInformation: string | null
  passenger: boolean | null
}
