import type {IOrderItem} from "@/interface/Order/IOrderItem";
import type {IValidOrderAddress} from "@/interface/OrderAddress/IValidOrderAddress";
import type {IOrderExtra} from "@/interface/Order/IOrderExtra";

export interface IOrder {
  cartToken: string | undefined
  createdAt: Date | undefined
  id: string | undefined
  items: IOrderItem[]
  updatedAt: Date | undefined
  _id: string | undefined
  itemsValidate: boolean | undefined
  addressValidate: boolean | undefined
  transport: Partial<IValidOrderAddress>
  extra: Partial<IOrderExtra>
  price: number | undefined
  startDate: Date | null
  endDate: Date | null
}
