export interface IOrderItem {
  createdAt: Date
  id: string
  name: string
  length: string
  width: string
  height: string
  weight: string
  orderId: string
  quantity: number
  updatedAt: Date
}
