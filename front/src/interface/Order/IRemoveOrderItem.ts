import type {IOrder} from "@/interface/Order/IOrder";
import type {IOrderItem} from "@/interface/Order/IOrderItem";

export interface IRemoveOrderItem extends IOrder {
  removeItem: IOrderItem
}
