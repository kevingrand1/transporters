// @ts-ignore
import fr from './fr.json'

export const defaultLocale = 'fr'

export const languages = {
  fr: fr,
}
