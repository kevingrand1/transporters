import {defineStore} from "pinia";
import type {IUserStore} from "@/interface/IUserStore";
import type {ILogin} from "@/interface/ILogin";
import {useCookies} from "vue3-cookies";

const initialState: IUserStore = {
  user: {
    address: '',
    email: '',
    enabled: false,
    firstname: '',
    fullName: '',
    humanizeRole: '',
    _id: Object,
    lastname: '',
    password: '',
    roles: [],
    salt: '',
    phone: '',
    isAdmin: false,
    isIndividual: false,
    isRetailer: false,
    isDeliveryman: false
  },
  token: '',
  authenticated: false
}

export const userStore = defineStore('userStore', {
  state: (): IUserStore => (initialState),
  actions: {
    login(user: ILogin) {
      const {cookies} = useCookies()
      cookies.set('token', user.token)
      Object.assign(this, user)
      this.authenticated = true;
    },
    logout() {
      const {cookies} = useCookies()
      cookies.remove('token')

      Object.assign(this, initialState)
      this.authenticated = false;
    }
  }
})
