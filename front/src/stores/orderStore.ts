import {defineStore} from "pinia";
import type {IOrder} from "@/interface/Order/IOrder";
import type {IOrderItem} from "@/interface/Order/IOrderItem";

const initialState: IOrder = {
  _id: undefined,
  cartToken: undefined,
  createdAt: undefined,
  id: undefined,
  items: [],
  updatedAt: undefined,
  itemsValidate: false,
  addressValidate: false,
  transport: {
    status: undefined,
    fromAddress: {
      address: {
        city: null,
        postcode: null,
        street: null,
        housenumber: null,
        fullAddress: null,
      },
      flexible: false,
      help: false
    },
    toAddress: {
      address: {
        city: null,
        postcode: null,
        street: null,
        housenumber: null,
        fullAddress: null,
      },
      flexible: false,
      help: false
    },
  },
  extra: {
    extraInformation: null,
    passenger: null
  },
  price: 0,
  startDate: null,
  endDate: null
}

export const useOrderStore = defineStore('useOrderStore', {
  state: () => initialState,
  actions: {
    setOrder(order: IOrder) {
      Object.assign(this, order)
    },
    addItem(item: IOrderItem) {
      this.items.push(item)
    },
    removeItem(item: IOrderItem) {
      const index = this.items.indexOf(item)
      if (index !== undefined) this.items.splice(index, 1)
    },
    updatePrice(price: number) {
      this.price = price
    }
  }
})
