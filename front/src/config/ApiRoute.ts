export const authPrefix = '/auth';
export const userPrefix = '/user';
export const ApiRoute = {
  auth: {
    login: authPrefix + '/login',
    register: authPrefix + '/register',
    refreshToken: authPrefix + '/refresh-token',
    updatePassword: authPrefix + '/update-password',
    updateInfo: authPrefix + '/update-info',
  },
  user: {
    deliverList: userPrefix + '/deliver-man/list',
    suppliersAndConsumers: userPrefix + '/retailer-and-individual/list',
    rolesList: userPrefix + '/roles-list',
    userStatistics: userPrefix + '/user-statistics',
  },
  order: {
    getOrInitiateOrder: '/order/get-or-initiate-order',
    getOrder: '/order/:cartToken',
    validateOrderExtra: '/order/validate-extra/:cartToken',
    updatePrice: '/order/update-price/:cartToken',
    updateDeliveryDate: '/order/change-delivery-date/:cartToken',
    getAvailableDelivery: '/order/available-delivery',
    items: {
      addItem: '/order/:cartToken/items/add',
      deleteItem: '/order/:cartToken/items/delete/:itemId',
      check: '/order/:cartToken/items/check',
      weightList: '/order/:cartToken/items/weight/list',
    }
  },
  orderAddress: {
    getStatusList: '/order/address/status',
    validateAddress: '/order/address/validate-address/:cartToken',
  }
}
