export const HomeRoute = 'home'
export const AdsRoute = 'ads'
export const CartRoute = 'cart'
export const InProgressRoute = 'in-progress'
export const ProfileRoute = 'profile'
export const LoginRoute = 'login'
export const RegisterRoute = 'register'

// ADMIN ROUTE
export const AdminRoute = 'admin'
export const AdminHomeRoute = 'admin-home'
export const AdminDeliveryDriversRoute = 'admin-delivery-drivers'
export const AdminSuppliersRoute = 'admin-suppliers'
export const AdminSupplierRoute = 'admin-dev-supplier'
export const AdminDelivererRoute = 'admin-dev-deliverer'

// RIDE ROUTE
export const RideRoute = 'ride'
export const RideSearchRoute = 'ride-search'
export const RideNewItemsRoute = 'ride-new-items'
export const RideNewTransportRoute = 'ride-new-transport'
export const RideNewExtraRoute = 'ride-new-extra'
export const RideNewPriceRoute = 'ride-new-price'
export const RideNewDatesRoute = 'ride-new-dates'

// SUPPLIER ROUTE
export const SupplierRoute = 'supplier'
export const SupplierAddProductRoute = 'supplier-add-product'

// DELIVERER ROUTE
export const DelivererRoute = 'deliverer'
export const DelivererHomeRoute = 'deliverer-home'
