export const ROLE_INDIVIDUAL = 'ROLE_INDIVIDUAL';
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_RETAILER = 'ROLE_RETAILER';
export const ROLE_DELIVERYMAN = 'ROLE_DELIVERYMAN';
export const ROLES_LIST = [ROLE_INDIVIDUAL, ROLE_ADMIN, ROLE_RETAILER, ROLE_DELIVERYMAN];
