start:
	@docker-compose -f docker-compose.yml up

stop:
	@docker-compose -f docker-compose.yml down

restart:
	@docker-compose -f docker-compose.yml restart

build:
	@docker-compose -f docker-compose.yml build

install:
	@docker-compose -f docker-compose.yml run --rm web npm install
	@docker-compose -f docker-compose.yml run --rm node_api npm install

back-bash:
	@docker-compose -f docker-compose.yml run --rm node_api sh

lint:
	@docker-compose -f docker-compose.yml run --rm web npm run lint
	@docker-compose -f docker-compose.yml run --rm node_api npm run lint

web-bash:
	@docker-compose -f docker-compose.yml run --rm web sh

launch:
	@make build
	@make install
	@make start

reload:
	@make stop
	@make launch

fixtures:
	@docker-compose -f docker-compose.yml run --rm node_api npm run fixtures
