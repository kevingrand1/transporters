
###### _Déborrah Brunier_ - _Guilhem Schira_ - _Baptiste Brand_ - _Kevin Grand_
<br>

# Challenge Stack - MongoDB - NodeJS - VueJS 
<br>
<br>

## Consignes :

_Pour sur positionner par rapport à son concurrent direct qui s’est positionné sur les VTC et la livraison uniquement de tacos et
autres plats riche en calories à des étudiants affamés en hypoglycémie, Uphonse est un service de livraison RAPIDE (vous
aurez compris le jeu de mot) qui permet de transporter absolument tout (ou presque)._
- [ ] Créer notre base de données Uphonse pour répondre aux besoins suivants :
- [ ] identifier les livreurs
- [ ] savoir de quel type de moyen de locomotion (sans fossile), ils utilisent
- [ ] savoir où ils sont basés
- [ ] savoir où ils sont localisés à un instant t, sur une durée limitée
- [ ] pour protéger la vie privée des livreurs, leur profil s’active d’un type “localisable” à true, s’ils souhaitent être pistés. S’il ne
- [ ] souhaite pas être pisté, cela signifie qu’il n’est pas disponible pour une livraison.
- [ ] identifier des clients de type “fournisseurs” c'est-à-dire ceux qui nécessitent de faire livrer quelque chose.
- [ ] identifier les clients qui vont recevoir les marchandises.
- [ ]  Peut-être, faut-il créer une catégorie produit, si c’est vraiment nécessaire.
- [ ] En tout cas, il faut créer une commande qui définit la relation entre fournisseurs et un client.
- [ ] La mise en place d’un historique sur une durée limitée (par exemple, un an

### Installation du projet
```shell
$ git clone https://gitlab.com/kevingrand1/transporters.git

$ cd transporter
```

### Installation de l'environnement de développement
```shell
$ cp .env.dist .env
$ cp .env_root_db.dist .env_root_db
```

### Initialisation de la base de données Transporter

#### Aller dans le docker-compose.yml puis commenter la ligne `command: bash -c 'mongod --bind_ip_all' de mongodb_`
```shell
$ docker-compose run mongodb

$ docker-compose exec mongodb sh

$ mongosh -u admin -p # (admin)

$ use admin

$ db.createUser(
      {
        user: "transporter",
        pwd: "transporter",
        roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
      }
   )
 # psw: DB_PASSWORD du .env   
 
$ use transporter

 # psw: DB_PASSWORD du .env   
$ db.createUser({user:"transporter", pwd: "transporter", roles:['dbOwner']})

# fermer les containers
$ make stop

# docker-compose.yml puis DECOMMENTER la ligne `command: bash -c 'mongod --bind_ip_all' de mongodb_`
```

### Lancement du projet
```shell
$ make launch

# URL Vue.js http://localhost:8080/
# URL Express.js http://localhost/api/v1
```

### Liste des commandes
```shell
# Lance tous les contenaires
$ make start
# Arrête tous les contenaires
$ make stop
# Redémarre tous les contenaires
$ make restart
# Construit tous les dockerfile
$ make build
# Entre dans le contenair d'express
$ make back_shell
# Lance les lint du front et du back
$ make lint
```


### Compte admin
- admin@gmail.com - 123456

