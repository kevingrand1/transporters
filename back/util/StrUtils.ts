import * as util from 'util';

/**
 * @public
 * @param {string} content
 * @param {Record<string, string>} params
 * @returns {string}
 * @description Replace string with params
 * @example
 * replace('Hello {name}', { name: 'World' }) // Hello World
 */
export const replace = (
  content: string,
  params: Record<string, string>
): string => {
  let result = content;
  for (const key in params) {
    result = result.replace(
      new RegExp(util.format('{%s}', key), 'gi'),
      params[key]
    );
  }
  return result;
};

export const slugify = (name: string): string => {
  return name
    .toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');
};
