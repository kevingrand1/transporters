export const omit = (obj: object, keys: string[] = ['id', '_id']) => {
  const newObj = {};
  for (const key in obj) {
    if (!keys.includes(key)) {
      newObj[key] = obj[key];
    }
  }
  return newObj;
};
