export const API_ROUTE = process.env.API_ROUTE;
export const BACK_PORT = process.env.BACK_PORT;
export const MONGO_PORT = process.env.MONGO_PORT;
export const MONGO_CONTAINER_NAME = process.env.MONGO_CONTAINER_NAME;
export const MONGO_DATABASE = process.env.MONGO_DATABASE;
export const MONGO_USERNAME = process.env.MONGO_USERNAME;
export const MONGO_PASSWORD = process.env.MONGO_PASSWORD;
