import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent
} from 'typeorm';
import { UserEntity } from '../entity/user.entity';
import AuthService from '../service/AuthService';
import { UpdateEvent } from 'typeorm/subscriber/event/UpdateEvent';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<UserEntity> {
  public async beforeInsert(
    event: InsertEvent<UserEntity>,
    authService: AuthService = new AuthService()
  ): Promise<void> {
    if (!(event.entity instanceof UserEntity)) return;
    const user = event.entity;
    await authService.hashPassword(user);
  }

  public async beforeUpdate(event: UpdateEvent<UserEntity>): Promise<void> {
    if (!(event.entity instanceof UserEntity)) return;
    const user = event.entity;
    user.updatedAt = new Date();
  }
}
