import UserRepository from '../repository/UserRepository';
import DefaultApiService from './DefaultApiService';
import { Request } from 'express';
import { PaginationDto } from '../model/pagination.dto';
import UserService from '../service/UserService';
import { UserEntity } from '../entity/user.entity';

export class UserApiService extends DefaultApiService {
  public constructor(private userRepository = UserRepository) {
    super();
  }

  public getDeliveryManList(request: Request, pagination: PaginationDto) {
    return this.userRepository.getDeliveryManList(pagination);
  }

  public async getRetailerAndIndividual(
    request: Request,
    pagination: PaginationDto
  ) {
    return this.userRepository.getRetailerAndIndividual(pagination);
  }

  public getRolesList() {
    return UserService.getRoles();
  }

  public async getUserStatistics() {
    const deliveryman = await this.userRepository.countByRole([
      UserEntity.ROLE_DELIVERYMAN
    ]);
    const clients = await this.userRepository.countByRole([
      UserEntity.ROLE_RETAILER,
      UserEntity.ROLE_INDIVIDUAL
    ]);
    const total = await this.userRepository.count();
    return { deliveryman, clients, total };
  }
}
