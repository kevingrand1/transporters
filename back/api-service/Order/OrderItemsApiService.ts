import DefaultApiService from '../DefaultApiService';
import { OrderItemService } from '../../service/Order/OrderItemService';
import { OrderItem } from '../../model/OrderItem';
import { IItemWeightSelect } from '../../interface/select/IItemWeightSelect';
import OrderRepository from '../../repository/OrderRepository';
import { ApiException } from '../../exception/ApiException';
import { ValidOrderItemDto } from '../../model/order/valid-order-item.dto';
import { OrderEntity } from '../../entity/order.entity';

export class OrderItemsApiService extends DefaultApiService {
  public constructor(
    private orderItemService: OrderItemService = new OrderItemService(),
    private orderRepository = OrderRepository
  ) {
    super();
  }

  public async getItemsWeightList(): Promise<IItemWeightSelect[]> {
    return OrderItem.CHOICES_WEIGHT.map((weight) => ({
      value: weight,
      label: this.orderItemService.humanizeWeight(weight),
      description: this.orderItemService.humanizeWeightDescription(weight)
    })).sort();
  }

  public async addItemToOrder(request: Request, cartToken: string) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }

    return {
      newItem: await this.orderItemService.addItemToOrder(order),
      ...order
    };
  }

  public async deleteItemFromOrder(
    request: Request,
    cartToken: string,
    itemId: string
  ) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }

    return this.orderItemService.deleteItemFromOrder(order, itemId);
  }

  public async checkOrderItems(
    request: Request,
    cartToken: string,
    items: ValidOrderItemDto[]
  ): Promise<OrderEntity> {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }

    if (!items || !items.length) {
      throw new ApiException('Items are required');
    }

    return this.orderItemService.checkOrderItems(order, items);
  }
}
//OrderApiService
// addItemToOrder
