import DefaultApiService from '../DefaultApiService';
import { Request } from 'express';
import OrderService from '../../service/Order/OrderService';
import { OrderEntity } from '../../entity/order.entity';
import { ValidOrderExtraDto } from '../../model/order/valid-order-extra.dto';
import { ApiException } from '../../exception/ApiException';
import OrderRepository from '../../repository/OrderRepository';
import { OrderExtraService } from '../../service/Order/OrderExtraService';
import { UserEntity } from '../../entity/user.entity';

export class OrderApiService extends DefaultApiService {
  public constructor(
    private orderService: OrderService = new OrderService(),
    private orderRepository = OrderRepository,
    private orderExtraService: OrderExtraService = new OrderExtraService()
  ) {
    super();
  }

  public async getOrInitiateOrder(
    request: Request,
    cartToken: string
  ): Promise<OrderEntity> {
    return this.orderService.getOrderOrInitiate(cartToken);
  }

  public async getOrder(
    request: Request,
    cartToken: string
  ): Promise<OrderEntity> {
    if (!cartToken) {
      throw new ApiException('Cart token is required');
    }

    return this.orderService.getOrderByCartToken(cartToken);
  }

  public async validateExtra(
    request: Request,
    cartToken: string,
    data: ValidOrderExtraDto
  ) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }
    return await this.orderExtraService.insertValues(data, order);
  }

  public async updatePrice(request: Request, cartToken: string, price: number) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }
    return await this.orderService.updatePrice(order, price);
  }

  public async updateDeliveryDate(
    request: Request,
    cartToken: string,
    startDate: Date,
    endDate: Date,
    user: UserEntity
  ) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }
    return await this.orderService.updateDeliveryDate(
      order,
      startDate,
      endDate,
      user
    );
  }

  public async findAvailableDelivery(
    request: Request,
    limit: number
  ): Promise<OrderEntity> {
    return this.orderRepository.findAvailableDelivery(limit);
  }
}
