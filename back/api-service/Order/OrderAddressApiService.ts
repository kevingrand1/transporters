import DefaultApiService from '../DefaultApiService';
import { OrderTransportService } from '../../service/Order/OrderTransportService';
import { OrderTransport } from '../../model/OrderTransport';
import { IDefaultSelectList } from '../../interface/select/IDefaultSelectList';
import { ValidOrderAddressDto } from '../../model/orderAddress/valid-order-address.dto';
import { ApiException } from '../../exception/ApiException';
import OrderRepository from '../../repository/OrderRepository';
import { OrderEntity } from '../../entity/order.entity';

export class OrderAddressApiService extends DefaultApiService {
  public constructor(
    private orderAddressService: OrderTransportService = new OrderTransportService(),
    private orderRepository = OrderRepository
  ) {
    super();
  }

  getAddressStatusList(): IDefaultSelectList[] {
    return OrderTransport.STATUS_LIST.map((status) => ({
      value: status,
      label: this.orderAddressService.humanizeStatus(status)
    })).sort();
  }

  public async validateAddress(
    request: Request,
    data: ValidOrderAddressDto,
    cartToken: string
  ): Promise<OrderEntity> {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) {
      throw new ApiException('Order not found');
    }

    return await this.orderAddressService.insertValues(data, order);
  }
}
