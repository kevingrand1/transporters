import { Request } from 'express';
import { ApiException } from '../exception/ApiException';
import AuthService from '../service/AuthService';
import { LoginDto } from '../model/auth/login.dto';
import DefaultApiService from './DefaultApiService';
import UserRepository from '../repository/UserRepository';
import { ObjectId } from 'mongodb';
import { RegisterDto } from '../model/auth/register.dto';
import { UserEntity } from '../entity/user.entity';
import { UpdatePasswordDto } from '../model/user/update-password.dto';
import { UpdateUserDto } from '../model/user/update-user.dto';

export class AuthApiService extends DefaultApiService {
  public constructor(
    private authService: AuthService = new AuthService(),
    private userRepository = UserRepository
  ) {
    super();
  }

  public async loginUser(request: Request, loginData: LoginDto) {
    const user = await this.userRepository.findOneByEmail(loginData.email);

    const checkPassword = user
      ? await this.authService.verifyPassword(user.password, loginData.password)
      : false;

    if (!checkPassword)
      throw new ApiException('auth.login.invalid_credentials');

    return {
      user: user,
      token: await this.authService.generateToken({
        email: user.email,
        _id: user._id
      })
    };
  }

  public async registerUser(request: Request, user: RegisterDto) {
    const checkExistingUserByEmail = await this.userRepository.findOneByEmail(
      user.email
    );

    if (checkExistingUserByEmail)
      throw new ApiException('auth.register.email_already_exists');

    const checkExistingUserByPhone = await this.userRepository.findOneByPhone(
      user.phone
    );

    if (checkExistingUserByPhone)
      throw new ApiException('auth.register.phone_already_exists');

    this.authService.checkPassword(user.password, user.confirmPassword);
    await this.authService.hashPassword(user);
    await this.userRepository.insert(user);
    return this.successMessage('auth.register.success');
  }

  public async refreshToken(request: Request) {
    const token = request.headers.authorization.split(' ')[1];
    const decoded = this.authService.decodeToken(token);

    const user = await this.userRepository.findOneBy({
      _id: new ObjectId(decoded._id)
    });

    return {
      user: user,
      token: await this.authService.generateToken({
        email: user.email,
        _id: user._id
      })
    };
  }

  public async updateUserInfo(
    request: Request,
    currentUser: UserEntity,
    userData: UpdateUserDto
  ) {
    await this.userRepository.update(currentUser._id, userData);
    const user = await this.userRepository.findOneBy({
      _id: new ObjectId(currentUser._id)
    });
    return {
      user,
      token: await this.authService.generateToken({
        email: user.email,
        _id: user._id
      })
    };
  }

  public async updateUserPassword(
    request: Request,
    currentUser: UserEntity,
    passwordData: UpdatePasswordDto
  ) {
    this.authService.checkPassword(
      passwordData.password,
      passwordData.confirmPassword
    );
    currentUser.password = passwordData.password;
    await this.authService.hashPassword(currentUser);
    await this.userRepository.update(currentUser._id, currentUser);
    const user = await this.userRepository.findOneBy({
      _id: new ObjectId(currentUser._id)
    });
    return {
      user,
      token: await this.authService.generateToken({
        email: user.email,
        _id: user._id
      })
    };
  }
}
