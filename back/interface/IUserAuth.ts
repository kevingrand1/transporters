export interface IUserAuth {
  password: string;
  salt: string;
}
