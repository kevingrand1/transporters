import { IDefaultSelectList } from './IDefaultSelectList';

export interface IItemWeightSelect extends IDefaultSelectList {
  description: string;
}
