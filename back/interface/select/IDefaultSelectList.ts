export interface IDefaultSelectList {
  value: string;
  label: string;
}
