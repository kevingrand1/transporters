import { IUserAuth } from '../../interface/IUserAuth';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdatePasswordDto implements IUserAuth {
  @IsNotEmpty()
  @IsString()
  public password: string;

  @IsNotEmpty()
  @IsString()
  public confirmPassword: string;

  public salt: string;
}
