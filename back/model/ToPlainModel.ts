import { instanceToPlain } from 'class-transformer';

export class ToPlainModel {
  public toJSON(): Record<string, unknown> {
    return instanceToPlain(this);
  }
}
