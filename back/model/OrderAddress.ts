import { Column } from 'typeorm';
import { Expose } from 'class-transformer';
import { ToPlainModel } from './ToPlainModel';

export class OrderAddress extends ToPlainModel {
  constructor(options?: Partial<OrderAddress>) {
    super();
    Object.assign(this, options);
  }

  @Column({ nullable: true })
  public city: string = null;

  @Column({ nullable: true })
  public postcode: string = null;

  @Column({ nullable: true })
  public street: string = null;

  @Column({ nullable: true })
  public housenumber: string = null;

  @Column({ nullable: true, type: 'float' })
  public x: number = null;

  @Column({ nullable: true, type: 'float' })
  public y: number = null;

  @Expose()
  public get fullAddress() {
    if (!this.street) return null;
    return `${this.housenumber} ${this.street} ${this.postcode} ${this.city}`;
  }
}
