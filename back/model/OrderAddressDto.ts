import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class OrderAddressDto {
  @IsNotEmpty()
  @IsString()
  public city: string;

  @IsNotEmpty()
  @IsString()
  public postcode: string;

  @IsNotEmpty()
  @IsString()
  public street: string;

  @IsNotEmpty()
  @IsString()
  public housenumber: string;

  @IsNotEmpty()
  @IsString()
  public x: number;

  @IsNotEmpty()
  @IsNumber()
  public y: number;
}
