import { Column } from 'typeorm';

export class OrderExtraInformation {
  @Column({ nullable: true, type: 'text' })
  public extraInformation: string | null = null;

  @Column()
  public passenger = false;
}
