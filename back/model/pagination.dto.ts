import { IsNumber, IsOptional, Min } from 'class-validator';

export class PaginationDto {
  @IsOptional()
  @IsNumber()
  @Min(1)
  public page?: number;

  @IsOptional()
  @IsNumber()
  @Min(10)
  public limit?: number;

  @IsOptional()
  public search?: string;
}
