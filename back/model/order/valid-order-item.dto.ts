import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class ValidOrderItemDto {
  @IsNotEmpty()
  @IsNumber()
  public id: string;

  @IsNotEmpty()
  @IsString()
  public name: string;

  @IsNotEmpty()
  @IsNumber()
  public quantity: number;

  @IsNotEmpty()
  @IsString()
  public length: string;

  @IsNotEmpty()
  @IsString()
  public width: string;

  @IsNotEmpty()
  @IsString()
  public height: string;

  @IsNotEmpty()
  @IsString()
  public weight: string;
}
