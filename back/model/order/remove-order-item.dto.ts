import { IsNotEmpty, IsString } from 'class-validator';

export class RemoveOrderItemDto {
  @IsNotEmpty()
  @IsString()
  public cartToken: string;

  @IsNotEmpty()
  @IsString()
  public itemId: string;
}
