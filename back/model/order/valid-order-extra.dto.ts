import { IsBoolean, IsNotEmpty } from 'class-validator';

export class ValidOrderExtraDto {
  public extraInformation: string | null = null;

  @IsNotEmpty()
  @IsBoolean()
  public passenger = false;
}
