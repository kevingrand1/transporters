import { IsNotEmpty, IsString } from 'class-validator';

export class AddOrderItemDto {
  @IsNotEmpty()
  @IsString()
  public cartToken: string;
}
