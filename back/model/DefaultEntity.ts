import {
  BaseEntity,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn
} from 'typeorm';
import { instanceToPlain } from 'class-transformer';
import { snowflakeId } from '../service/EntityService';

export default abstract class DefaultEntity extends BaseEntity {
  @PrimaryColumn()
  public id: string;

  public constructor() {
    super();
    this.id = snowflakeId();
  }

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  public toJSON() {
    return instanceToPlain(this);
  }
}
