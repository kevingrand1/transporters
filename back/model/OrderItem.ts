import { Column } from 'typeorm';
import DefaultModel from './DefaultModel';

export class OrderItem extends DefaultModel {
  public static WEIGHT_LIGHT = 'WEIGHT_LIGHT';
  public static WEIGHT_MEDIUM = 'WEIGHT_MEDIUM';
  public static WEIGHT_HEAVY = 'WEIGHT_HEAVY';

  public static CHOICES_WEIGHT = [
    OrderItem.WEIGHT_LIGHT,
    OrderItem.WEIGHT_MEDIUM,
    OrderItem.WEIGHT_HEAVY
  ];

  public static WEIGHT_TO_KG = {
    [OrderItem.WEIGHT_LIGHT]: 0.5,
    [OrderItem.WEIGHT_MEDIUM]: 1,
    [OrderItem.WEIGHT_HEAVY]: 2
  };

  @Column({ default: 1 })
  public quantity = 1;

  @Column({ nullable: true })
  public name: string | null = null;

  @Column({ nullable: true })
  public length: number | null = null;

  @Column({ nullable: true })
  public width: number | null = null;

  @Column({ nullable: true })
  public height: number | null = null;

  @Column()
  public weight: number | null = null;
}
