import { Column } from 'typeorm';
import { instanceToInstance } from 'class-transformer';
import { snowflakeId } from '../service/EntityService';

export default abstract class DefaultModel {
  @Column()
  public id: string;

  public constructor() {
    this.id = snowflakeId();
  }

  public toJSON() {
    return instanceToInstance(this);
  }
}
