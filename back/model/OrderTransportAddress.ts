import { Column } from 'typeorm';
import { OrderAddress } from './OrderAddress';

export class OrderTransportAddress {
  @Column(() => OrderAddress)
  public address: OrderAddress;

  @Column()
  public flexible = false;

  @Column()
  public help = false;
}
