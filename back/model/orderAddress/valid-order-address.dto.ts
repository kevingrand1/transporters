import { IsEnum, IsNotEmpty, IsString, NotEquals } from 'class-validator';
import { ValidAddressDto } from './valid-address.dto';
import { OrderTransport } from '../OrderTransport';

export class ValidOrderAddressDto {
  @IsNotEmpty()
  @IsString()
  @IsEnum(OrderTransport.STATUS_LIST)
  public status: string;

  @NotEquals(null)
  public fromAddress: ValidAddressDto;

  @NotEquals(null)
  public toAddress: ValidAddressDto;
}
