import { OrderAddressDto } from '../OrderAddressDto';
import { IsBoolean } from 'class-validator';

export class ValidAddressDto {
  address: OrderAddressDto;

  @IsBoolean()
  flexible: boolean;

  @IsBoolean()
  help: boolean;
}
