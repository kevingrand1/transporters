import { Column } from 'typeorm';
import { OrderTransportAddress } from './OrderTransportAddress';

export class OrderTransport {
  public static STATUS_SENDER = 'sender';
  public static STATUS_RECIPIENT = 'recipient';

  public static STATUS_LIST = [
    OrderTransport.STATUS_SENDER,
    OrderTransport.STATUS_RECIPIENT
  ];

  @Column({ type: 'string', nullable: true })
  public status: string = null;

  @Column(() => OrderTransportAddress)
  public fromAddress: OrderTransportAddress;

  @Column(() => OrderTransportAddress)
  public toAddress: OrderTransportAddress;
}
