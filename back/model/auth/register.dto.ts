import { IsNotEmpty, IsString } from 'class-validator';
import { IUserAuth } from '../../interface/IUserAuth';
import { UpdateUserDto } from '../user/update-user.dto';

export class RegisterDto extends UpdateUserDto implements IUserAuth {
  @IsNotEmpty()
  @IsString()
  public password: string;

  @IsNotEmpty()
  @IsString()
  public confirmPassword: string;

  public salt: string;
}
