import * as argon2 from 'argon2';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import { IUserAuth } from '../interface/IUserAuth';
import { ApiException } from '../exception/ApiException';

export default class AuthService {
  public async hashPassword(user: IUserAuth) {
    const salt = crypto.randomBytes(32);
    const hash = await argon2.hash(user.password, { salt });
    user.password = hash;
    user.salt = salt.toString('hex');
    return { salt, hash };
  }

  public async verifyPassword(hash: string, password: string) {
    return await argon2.verify(hash, password);
  }

  public async generateToken(payload: Record<string, unknown>) {
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' });
  }

  public decodeToken(token: string): { _id: string } {
    return jwt.verify(token, process.env.JWT_SECRET) as { _id: string };
  }

  public checkPassword(password: string, confirmPassword: string) {
    if (password !== confirmPassword)
      throw new ApiException('Password not match');
  }
}
