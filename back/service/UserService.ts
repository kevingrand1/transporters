import { Action } from 'routing-controllers';
import AuthService from './AuthService';
import UserRepository from '../repository/UserRepository';
import { ObjectId } from 'mongodb';
import translator from '../util/TranslatorUtil';
import { UserEntity } from '../entity/user.entity';

export default class UserService {
  public constructor(
    private authService: AuthService = new AuthService(),
    private userRepository = UserRepository
  ) {}

  public currentUserChecker(action: Action) {
    const token = action.request.headers.authorization.split(' ')[1];
    const decoded = this.authService.decodeToken(token);
    return this.userRepository.findOneBy({
      _id: new ObjectId(decoded._id)
    });
  }

  public async authorizationChecker(action: Action, roles: string[]) {
    // && roles.includes(user.roles);
    return !!(await this.currentUserChecker(action));
  }

  public static humanizeRole(role: string) {
    const roles = {
      [UserEntity.ROLE_INDIVIDUAL]: 'Individual',
      [UserEntity.ROLE_ADMIN]: 'Administrator',
      [UserEntity.ROLE_DELIVERYMAN]: 'Delivery Man',
      [UserEntity.ROLE_RETAILER]: 'Retailer'
    };

    return translator(roles[role]);
  }

  public static getRoles() {
    return {
      default: UserEntity.ROLE_INDIVIDUAL,
      roles: UserEntity.CHOICES_ROLES.map((role) => ({
        value: role,
        label: this.humanizeRole(role)
      })).sort()
    };
  }
}
