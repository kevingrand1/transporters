import { ValidOrderExtraDto } from '../../model/order/valid-order-extra.dto';
import { OrderExtraInformation } from '../../model/OrderExtraInformation';
import { OrderEntity } from '../../entity/order.entity';

export class OrderExtraService {
  public initialValues(): OrderExtraInformation {
    return new OrderExtraInformation();
  }

  public async insertValues(data: ValidOrderExtraDto, order: OrderEntity) {
    order.extra.extraInformation = data.extraInformation;
    order.extra.passenger = data.passenger;
    await order.save();
    return order;
  }
}
