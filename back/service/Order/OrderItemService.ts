import { OrderItem } from '../../model/OrderItem';
import translator from '../../util/TranslatorUtil';
import { OrderEntity } from '../../entity/order.entity';
import { ValidOrderItemDto } from '../../model/order/valid-order-item.dto';
import { omit } from '../../util/ObjectUtil';

export class OrderItemService {
  public initialValues(): OrderItem[] {
    return [new OrderItem()];
  }

  public async addItemToOrder(order: OrderEntity): Promise<OrderItem> {
    const orderItem = new OrderItem();

    order.items = [...order.items, orderItem];
    await this.updateOrderItemQuantity(order);
    await order.save();
    return orderItem;
  }

  public async updateOrderItemQuantity(
    order: OrderEntity
  ): Promise<OrderEntity> {
    order.itemsQuantity = order.items.reduce(
      (acc, item) => acc + item.quantity,
      0
    );
    return order;
  }

  public async deleteItemFromOrder(
    order: OrderEntity,
    itemId: string
  ): Promise<OrderItem> {
    const removedItem = order.items.find((item) => item.id === itemId);
    order.items = order.items.filter((item) => item.id !== itemId);
    await this.updateOrderItemQuantity(order);
    await order.save();
    return removedItem;
  }

  public async checkOrderItems(
    order: OrderEntity,
    items: ValidOrderItemDto[]
  ): Promise<OrderEntity> {
    for (const item of items) {
      const dbItem = order.items.find((i) => i.id === item.id);
      dbItem.name = item.name;
      Object.assign(dbItem, omit(item));
    }

    order.itemsValidate = true;
    await order.save();

    return order;
  }

  public humanizeWeight(weight: string) {
    const weights = {
      [OrderItem.WEIGHT_LIGHT]: 'Light',
      [OrderItem.WEIGHT_MEDIUM]: 'Medium',
      [OrderItem.WEIGHT_HEAVY]: 'Heavy'
    };

    return translator(weights[weight]);
  }

  public humanizeWeightDescription(weight: string) {
    const weights = {
      [OrderItem.WEIGHT_LIGHT]: 'Less than 5kg',
      [OrderItem.WEIGHT_MEDIUM]: 'Between 5kg and 10kg',
      [OrderItem.WEIGHT_HEAVY]: 'More than 10kg'
    };

    return translator(weights[weight]);
  }
}
