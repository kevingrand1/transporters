import { OrderAddress } from '../../model/OrderAddress';
import { OrderTransport } from '../../model/OrderTransport';
import translator from '../../util/TranslatorUtil';
import { ValidOrderAddressDto } from '../../model/orderAddress/valid-order-address.dto';
import { OrderEntity } from '../../entity/order.entity';
import { OrderItem } from '../../model/OrderItem';

export class OrderTransportService {
  public async insertValues(
    data: ValidOrderAddressDto,
    order: OrderEntity
  ): Promise<OrderEntity> {
    order.transport.status = data.status;

    order.transport.fromAddress = {
      help: data.fromAddress.help,
      flexible: data.fromAddress.flexible,
      address: new OrderAddress({
        city: data.fromAddress.address.city,
        postcode: data.fromAddress.address.postcode,
        street: data.fromAddress.address.street,
        housenumber: data.fromAddress.address.housenumber,
        x: data.fromAddress.address.x,
        y: data.fromAddress.address.y
      })
    };

    order.transport.toAddress = {
      help: data.toAddress.help,
      flexible: data.toAddress.flexible,
      address: new OrderAddress({
        city: data.toAddress.address.city,
        postcode: data.toAddress.address.postcode,
        street: data.toAddress.address.street,
        housenumber: data.toAddress.address.housenumber,
        x: data.toAddress.address.x,
        y: data.toAddress.address.y
      })
    };

    order.price = this.calculatePrice(order);

    order.addressValidate = true;
    await order.save();
    return order;
  }

  public initialValues(): OrderTransport {
    const fromAddress = new OrderAddress();
    const toAddress = new OrderAddress();
    const transport = new OrderTransport();
    transport.toAddress = {
      help: null,
      flexible: null,
      address: toAddress
    };
    transport.fromAddress = {
      help: null,
      flexible: null,
      address: fromAddress
    };
    return transport;
  }

  public humanizeStatus(status: string) {
    const statusList = {
      [OrderTransport.STATUS_SENDER]: 'Shipper',
      [OrderTransport.STATUS_RECIPIENT]: 'Recipient'
    };

    return translator(statusList[status]);
  }

  public calculateDistance(fromAddress: OrderAddress, toAddress: OrderAddress) {
    const distanceEnMetres = Math.sqrt(
      Math.pow(toAddress.x - fromAddress.x, 2) +
        Math.pow(toAddress.y - fromAddress.y, 2)
    );
    return distanceEnMetres / 1000;
  }

  public calculatePrice(order: OrderEntity) {
    const distance = this.calculateDistance(
      order.transport.fromAddress.address,
      order.transport.toAddress.address
    );

    const multiplier = order.items.reduce((acc, item) => {
      return acc + (item.quantity + OrderItem.WEIGHT_TO_KG[item.weight]);
    }, 0);

    return Math.round((0.12 + multiplier * 0.031) * distance);
  }
}
