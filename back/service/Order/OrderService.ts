import { OrderEntity } from '../../entity/order.entity';
import OrderRepository from '../../repository/OrderRepository';
import { OrderExtraService } from './OrderExtraService';
import { OrderItemService } from './OrderItemService';
import { OrderTransportService } from './OrderTransportService';
import { ApiException } from '../../exception/ApiException';
import { UserEntity } from '../../entity/user.entity';

export default class OrderService {
  constructor(
    private orderRepository = OrderRepository,
    private orderItemService: OrderItemService = new OrderItemService(),
    private orderTransportService: OrderTransportService = new OrderTransportService(),
    private orderExtraService: OrderExtraService = new OrderExtraService()
  ) {}

  public async getOrderByCartToken(cartToken: string) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) throw new ApiException('Order not found');
    return order;
  }

  public async getAllOrders() {
    const orders = await this.orderRepository.getAllOrders();
    if (!orders) throw new ApiException('Orders not found');
    return orders;
  }

  public async getOrderOrInitiate(cartToken: string) {
    const order = await this.orderRepository.getOrderByCartToken(cartToken);
    if (!order) return await this.initializeOrder();
    return order;
  }

  public async initializeOrder(): Promise<OrderEntity> {
    const order = new OrderEntity();
    // order.extra = this.orderExtraService.initialValues();
    order.transport = this.orderTransportService.initialValues();
    order.items = this.orderItemService.initialValues();
    order.extra = this.orderExtraService.initialValues();
    await order.save();
    return order;
  }

  public async updatePrice(order: OrderEntity, price: number) {
    order.price = price;
    await order.save();
    return order;
  }

  public async updateDeliveryDate(
    order: OrderEntity,
    startDate: Date,
    endDate: Date,
    user: UserEntity
  ) {
    order.startDate = startDate;
    order.endDate = endDate;
    order.userId = user.id;
    order.status = OrderEntity.STATUS_PENDING;
    await order.save();
    return order;
  }
}
