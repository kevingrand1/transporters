import { IProcessor } from 'typeorm-fixtures-cli';
import { OrderEntity } from '../entity/order.entity';
import { OrderItem } from '../model/OrderItem';
import { generateRandomNumber } from '../util/NumberUtil';

export default class OrderEntityProcessor implements IProcessor<OrderEntity> {
  private generateOrderItems(length: number): OrderItem[] {
    const items: OrderItem[] = [];
    for (let i = 0; i < length; i++) {
      const item = new OrderItem();
      item.height = generateRandomNumber(1, 100);
      item.length = generateRandomNumber(1, 100);
      item.weight = generateRandomNumber(1, 100);
      item.width = generateRandomNumber(1, 100);
      item.name = `Item ${i}`;
      item.quantity = generateRandomNumber(1, 10);
      items.push(item);
    }
    return items;
  }
  public preProcess(name: string, object: any): OrderEntity {
    if (object.items || object.items == 0) {
      object.items = this.generateOrderItems(
        object.items == 0 ? 1 : object.items
      );
      object.itemsQuantity = object.items.length;
    }

    if (object.startDate) {
      object.startDate = new Date(object.startDate);
    }

    if (object.endDate) {
      object.endDate = new Date(object.endDate);
    }

    return object;
  }
}
