import { AppDataSource } from '../data-source';
import { UserEntity } from '../entity/user.entity';
import { PaginationDto } from '../model/pagination.dto';

const basePagination = (pagination: PaginationDto) => {
  return {
    take: pagination.limit,
    skip: (pagination.page - 1) * pagination.limit
  };
};

const UserRepository = AppDataSource.getRepository(UserEntity).extend({
  async getDeliveryManList(pagination: PaginationDto) {
    return await this.findAndCount({
      where: {
        roles: {
          $in: [UserEntity.ROLE_DELIVERYMAN]
        }
      },
      ...basePagination(pagination)
    });
  },

  async getRetailerAndIndividual(pagination: PaginationDto) {
    return await this.findAndCount({
      where: {
        roles: {
          $in: [UserEntity.ROLE_RETAILER, UserEntity.ROLE_INDIVIDUAL]
        }
      },
      ...basePagination(pagination)
    });
  },

  async findOneByEmail(email: string) {
    return await this.findOne({
      where: {
        email
      }
    });
  },

  async findOneByPhone(phone: string) {
    return await this.findOne({
      where: {
        phone
      }
    });
  },

  async countByRole(roles: string[]) {
    return this.countBy({
      roles: {
        $in: roles
      }
    });
  }
});

export default UserRepository;
