import { AppDataSource } from '../data-source';
import { OrderEntity } from '../entity/order.entity';

const OrderRepository = AppDataSource.getRepository(OrderEntity).extend({
  getOrderByCartToken(cartToken: string): Promise<OrderEntity> {
    return this.findOneBy({ cartToken });
  },
  getAllOrders(): Promise<OrderEntity> {
    return this.find();
  },
  findAvailableDelivery(limit: number | null = null): Promise<OrderEntity> {
    const params = {
      where: {
        status: { $eq: OrderEntity.STATUS_PENDING },
        endDate: { $gte: new Date() }
      },
      order: {
        updatedAt: 'ASC'
      }
    };
    if (limit) params['limit'] = limit;
    return this.find(params);
  }
});

export default OrderRepository;
