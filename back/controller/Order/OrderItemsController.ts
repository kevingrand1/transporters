import {
  Body,
  Delete,
  Get,
  JsonController,
  Param,
  Post,
  Req
} from 'routing-controllers';
import { BaseController } from '../BaseController';
import { OrderItemsApiService } from '../../api-service/Order/OrderItemsApiService';
import { Request } from 'express';
import { OrderItem } from '../../model/OrderItem';

@JsonController('/order/:cartToken/items')
export default class OrderItemsController extends BaseController {
  @Get('/weight/list')
  public getItemsWeightList(@Req() request: Request) {
    return this.handleRequest(request, {
      service: OrderItemsApiService,
      fn: 'getItemsWeightList'
    });
  }

  @Post('/add')
  public addItemToOrder(
    @Req() request: Request,
    @Param('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderItemsApiService,
      fn: 'addItemToOrder',
      args: [cartToken]
    });
  }

  @Delete('/delete/:itemId')
  public deleteItemFromOrder(
    @Req() request: Request,
    @Param('cartToken') cartToken: string,
    @Param('itemId') itemId: string
  ) {
    return this.handleRequest(request, {
      service: OrderItemsApiService,
      fn: 'deleteItemFromOrder',
      args: [cartToken, itemId]
    });
  }

  @Post('/check')
  public checkOrderItems(
    @Req() request: Request,
    @Body() items: OrderItem[],
    @Param('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderItemsApiService,
      fn: 'checkOrderItems',
      args: [cartToken, items]
    });
  }
}
