import {
  Body,
  Get,
  JsonController,
  Param,
  Post,
  Req
} from 'routing-controllers';
import { BaseController } from '../BaseController';
import { Request } from 'express';
import { OrderAddressApiService } from '../../api-service/Order/OrderAddressApiService';
import { ValidOrderAddressDto } from '../../model/orderAddress/valid-order-address.dto';

@JsonController('/order/address')
export default class OrderAddressController extends BaseController {
  @Get('/status')
  public getAddressStatusList(@Req() request: Request) {
    return this.handleRequest(request, {
      service: OrderAddressApiService,
      fn: 'getAddressStatusList'
    });
  }

  @Post('/validate-address/:cartToken')
  public validateAddress(
    @Req() request: Request,
    @Body() data: ValidOrderAddressDto,
    @Param('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderAddressApiService,
      fn: 'validateAddress',
      args: [data, cartToken]
    });
  }
}
