import {
  Body,
  BodyParam,
  CurrentUser,
  Get,
  JsonController,
  Param,
  Post,
  Req
} from 'routing-controllers';
import { BaseController } from '../BaseController';
import { OrderApiService } from '../../api-service/Order/OrderApiService';
import { Request } from 'express';
import { ValidOrderExtraDto } from '../../model/order/valid-order-extra.dto';

@JsonController('/order')
export default class OrderController extends BaseController {
  @Post('/get-or-initiate-order')
  public getOrInitiateOrder(
    @Req() request: Request,
    @BodyParam('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'getOrInitiateOrder',
      args: [cartToken]
    });
  }

  @Post('/validate-extra/:cartToken')
  public validateExtra(
    @Req() request: Request,
    @Body() extra: ValidOrderExtraDto,
    @Param('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'validateExtra',
      args: [cartToken, extra]
    });
  }

  @Post('/update-price/:cartToken')
  public updatePrice(
    @Req() request: Request,
    @Param('cartToken') cartToken: string,
    @BodyParam('price') price: number
  ) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'updatePrice',
      args: [cartToken, price]
    });
  }

  @Post('/change-delivery-date/:cartToken')
  public changeDeliveryDate(
    @Req() request: Request,
    @Param('cartToken') cartToken: string,
    @BodyParam('startDate') startDate: Date,
    @BodyParam('endDate') endDate: Date,
    @CurrentUser() user: any
  ) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'updateDeliveryDate',
      args: [cartToken, startDate, endDate, user]
    });
  }

  @Post('/available-delivery')
  public getOrders(@Req() request: Request, @BodyParam('limit') limit: string) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'findAvailableDelivery',
      args: [limit]
    });
  }

  @Get('/:cartToken')
  public getOrder(
    @Req() request: Request,
    @Param('cartToken') cartToken: string
  ) {
    return this.handleRequest(request, {
      service: OrderApiService,
      fn: 'getOrder',
      args: [cartToken]
    });
  }
}
