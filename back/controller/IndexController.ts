import { Response } from 'express';
import { Get, JsonController, Res } from 'routing-controllers';

@JsonController('/test')
class HomeController {
  @Get('/test')
  async get(@Res() response: Response) {
    return response.json({ status: '1' });
  }
}

export default HomeController;
