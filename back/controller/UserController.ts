import { Request } from 'express';
import { Post, JsonController, Req, Body } from 'routing-controllers';

import { BaseController } from './BaseController';
import { UserApiService } from '../api-service/UserApiService';
import { PaginationDto } from '../model/pagination.dto';

@JsonController('/user')
class UserController extends BaseController {
  @Post('/deliver-man/list')
  public async getDeliveryManList(
    @Req() request: Request,
    @Body() pagination: PaginationDto
  ) {
    return this.handleRequest(request, {
      service: UserApiService,
      fn: 'getDeliveryManList',
      args: [pagination]
    });
  }

  @Post('/retailer-and-individual/list')
  public async getRetailerAndIndividual(
    @Req() request: Request,
    @Body() pagination: PaginationDto
  ) {
    return this.handleRequest(request, {
      service: UserApiService,
      fn: 'getRetailerAndIndividual',
      args: [pagination]
    });
  }

  @Post('/roles-list')
  public async getRolesList(@Req() request: Request) {
    return this.handleRequest(request, {
      service: UserApiService,
      fn: 'getRolesList',
      args: []
    });
  }

  @Post('/user-statistics')
  public async getUserStatistics(@Req() request: Request) {
    return this.handleRequest(request, {
      service: UserApiService,
      fn: 'getUserStatistics',
      args: []
    });
  }
}

export default UserController;
