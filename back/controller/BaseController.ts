import { Request } from 'express';
import { ApiException } from '../exception/ApiException';
import DefaultApiService from '../api-service/DefaultApiService';

export class BaseController {
  protected async handleRequest(
    request: Request,
    args: { service: new () => DefaultApiService; fn: string; args?: unknown[] }
  ) {
    const service = args['service'] ? new args['service']() : null;
    const fn = args['fn'] ?? null;
    const fnArgs = args['args'] ?? [];

    if (!service && !fn) throw new ApiException('Aucun service demandé');
    try {
      return request.res.json(await service[fn](request, ...fnArgs));
    } catch (e) {
      if (e instanceof ApiException) {
        return request.res
          .status(e.code)
          .json({ message: e.message, success: false });
      }
      return request.res
        .status(500)
        .json({ message: e.message, success: false });
    }
  }
}
