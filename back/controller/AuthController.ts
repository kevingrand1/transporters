import { Request } from 'express';
import {
  Post,
  JsonController,
  Body,
  Req,
  Patch,
  CurrentUser
} from 'routing-controllers';

import { BaseController } from './BaseController';
import { AuthApiService } from '../api-service/AuthApiService';
import { LoginDto } from '../model/auth/login.dto';
import { RegisterDto } from '../model/auth/register.dto';
import { UserEntity } from '../entity/user.entity';
import { UpdatePasswordDto } from '../model/user/update-password.dto';
import { UpdateUserDto } from '../model/user/update-user.dto';

@JsonController('/auth')
class AuthController extends BaseController {
  @Post('/register')
  async registerUser(@Body() userData: RegisterDto, @Req() request: Request) {
    return this.handleRequest(request, {
      service: AuthApiService,
      fn: 'registerUser',
      args: [userData]
    });
  }

  @Post('/login')
  async loginUser(@Body() loginData: LoginDto, @Req() request: Request) {
    return this.handleRequest(request, {
      service: AuthApiService,
      fn: 'loginUser',
      args: [loginData]
    });
  }

  @Post('/refresh-token')
  async refreshToken(@Req() request: Request) {
    return this.handleRequest(request, {
      service: AuthApiService,
      fn: 'refreshToken',
      args: []
    });
  }

  @Patch('/update-info')
  public async updateUserInfo(
    @Req() request: Request,
    @CurrentUser({ required: true }) currentUser: UserEntity,
    @Body() userData: UpdateUserDto
  ) {
    return this.handleRequest(request, {
      service: AuthApiService,
      fn: 'updateUserInfo',
      args: [currentUser, userData]
    });
  }

  @Patch('/update-password')
  async updateUserPassword(
    @Req() request: Request,
    @Body({ required: true }) userData: UpdatePasswordDto,
    @CurrentUser({ required: true }) currentUser: UserEntity
  ) {
    return this.handleRequest(request, {
      service: AuthApiService,
      fn: 'updateUserPassword',
      args: [currentUser, userData]
    });
  }
}

export default AuthController;
