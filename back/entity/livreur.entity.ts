import { Entity, ObjectIdColumn, Column, BaseEntity } from 'typeorm';

@Entity('livreur')
export class Livreur extends BaseEntity {
  public static TRANSPORT_LIST = [];

  @ObjectIdColumn()
  id: number;

  @Column()
  nom: string;

  @Column()
  prenom: string;

  @Column()
  lieu: string;

  @Column()
  transport: string[];

  @Column({ default: false })
  position = false;

  @Column()
  geolocation: string;
}
