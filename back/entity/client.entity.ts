import { Entity, ObjectIdColumn, Column, BaseEntity } from 'typeorm';

@Entity('client')
export class Client extends BaseEntity {
  @ObjectIdColumn()
  id: number;

  @Column()
  nom: string;

  @Column()
  prenom: string;

  @Column()
  email: string;

  @Column()
  adresse: string;

  @Column()
  type: boolean;
}
