import DefaultEntity from '../model/DefaultEntity';
import {
  Column,
  Entity,
  Index,
  ObjectId,
  ObjectIdColumn,
  Unique
} from 'typeorm';
import ShortUniqueId from 'short-unique-id';
import { OrderItem } from '../model/OrderItem';
import { OrderTransport } from '../model/OrderTransport';
import { OrderExtraInformation } from '../model/OrderExtraInformation';
import { Expose } from 'class-transformer';

@Entity('order')
@Unique(['cartToken'])
export class OrderEntity extends DefaultEntity {
  public static STATUS_DRAFT = 'draft';
  public static STATUS_PENDING = 'pending';
  public static STATUS_PROCESSING = 'processing';
  public static STATUS_CANCELED = 'canceled';
  public static STATUS_COMPLETED = 'completed';

  public static STATUS_LIST = [
    OrderEntity.STATUS_DRAFT,
    OrderEntity.STATUS_PENDING,
    OrderEntity.STATUS_PROCESSING,
    OrderEntity.STATUS_CANCELED,
    OrderEntity.STATUS_COMPLETED
  ];

  @ObjectIdColumn()
  public _id: ObjectId;

  @Column()
  @Index()
  public cartToken: string;

  @Column({ type: 'text', default: OrderEntity.STATUS_DRAFT })
  public status: string = OrderEntity.STATUS_DRAFT;

  @Column({ default: false })
  public itemsValidate = false;

  @Column({ default: false })
  public addressValidate = false;

  @Column({ default: 1 })
  public itemsQuantity = 1;

  @Column({ nullable: true })
  public userId: string | null = null;

  @Column({ nullable: true })
  public price: number | null = null;

  @Column({ type: 'timestamptz' })
  public startDate: Date | null = null;

  @Column({ type: 'timestamptz' })
  public endDate: Date | null = null;

  @Column(() => OrderExtraInformation)
  public extra: OrderExtraInformation;

  @Column(() => OrderTransport)
  public transport: OrderTransport;

  @Column(() => OrderItem)
  public items: OrderItem[] = [];

  constructor() {
    super();
    this.cartToken = new ShortUniqueId().stamp(32);
  }

  @Expose()
  public itemsVolume(): string {
    const volumes = this.items.reduce((acc, item) => {
      return acc + item.quantity * (item.length * item.width * item.height);
    }, 0);
    return (volumes / 1000000).toFixed(2);
  }
}
