import { Entity, ObjectIdColumn, Column, BaseEntity } from 'typeorm';

@Entity('command')
export class Command extends BaseEntity {
  @ObjectIdColumn()
  id: number;

  @Column()
  date: Date;

  // RELATION CLIENT

  // RELATION LIVREUR

  // RELATION PRODUCT
}
