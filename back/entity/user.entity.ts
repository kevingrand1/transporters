import { Entity, ObjectId, ObjectIdColumn, Column, Unique } from 'typeorm';
import { Expose } from 'class-transformer';
import DefaultEntity from '../model/DefaultEntity';
import { IUserAuth } from '../interface/IUserAuth';
import UserService from '../service/UserService';

@Entity('user')
@Unique(['id', 'email'])
export class UserEntity extends DefaultEntity implements IUserAuth {
  public static ROLE_INDIVIDUAL = 'ROLE_INDIVIDUAL';
  public static ROLE_ADMIN = 'ROLE_ADMIN';
  public static ROLE_RETAILER = 'ROLE_RETAILER';
  public static ROLE_DELIVERYMAN = 'ROLE_DELIVERYMAN';

  public static CHOICES_ROLES = [
    UserEntity.ROLE_INDIVIDUAL,
    UserEntity.ROLE_RETAILER,
    UserEntity.ROLE_DELIVERYMAN
  ];

  @ObjectIdColumn()
  public _id: ObjectId;

  @Column()
  public firstname: string;

  @Column()
  public lastname: string;

  @Column()
  public email: string;

  @Column({ nullable: true })
  public phone: string = null;

  @Column()
  public password: string;

  @Column()
  public salt: string;

  @Column()
  public address: string;

  @Column({ type: 'text', array: true, default: UserEntity.ROLE_INDIVIDUAL })
  public roles: string[] = [UserEntity.ROLE_INDIVIDUAL];

  @Column({ default: false })
  public enabled = false;

  public toString() {
    return this.id;
  }

  @Expose()
  public get fullName() {
    return `${this.firstname} ${this.lastname}`;
  }

  @Expose()
  public humanizeRole() {
    return this.roles.map((role) => UserService.humanizeRole(role)).join(', ');
  }

  @Expose()
  public isAdmin() {
    return this.roles.includes(UserEntity.ROLE_ADMIN);
  }

  @Expose()
  public isIndividual() {
    return this.roles.includes(UserEntity.ROLE_INDIVIDUAL);
  }

  @Expose()
  public isRetailer() {
    return this.roles.includes(UserEntity.ROLE_RETAILER);
  }

  @Expose()
  public isDeliveryman() {
    return this.roles.includes(UserEntity.ROLE_DELIVERYMAN);
  }
}
