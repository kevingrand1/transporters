import { DataSource } from 'typeorm';
import {
  MONGO_CONTAINER_NAME,
  MONGO_DATABASE,
  MONGO_PASSWORD,
  MONGO_PORT,
  MONGO_USERNAME
} from './config/AppConfig';

export const AppDataSource = new DataSource({
  type: 'mongodb',
  host: MONGO_CONTAINER_NAME,
  port: parseInt(MONGO_PORT) || 27017,
  database: MONGO_DATABASE,
  username: MONGO_USERNAME,
  password: MONGO_PASSWORD,
  synchronize: true,
  logging: true,
  entities: ['entity/**/*.ts'],
  migrations: ['migration/**/*.ts'],
  subscribers: ['subscriber/**/*.ts']
});
