import 'reflect-metadata';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import { useExpressServer } from 'routing-controllers';
import { API_ROUTE, BACK_PORT } from './config/AppConfig';
import { AppDataSource } from './data-source';
import UserService from './service/UserService';
const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

AppDataSource.initialize().then(() => {
  console.log('Connected to database');

  useExpressServer(app, {
    development: process.env.APP_DEV === 'true',
    defaultErrorHandler: false,
    controllers: [__dirname + '/controller/**/*.ts'],
    currentUserChecker: async (action) =>
      new UserService().currentUserChecker(action),
    authorizationChecker: async (action, roles) =>
      new UserService().authorizationChecker(action, roles),
    routePrefix: API_ROUTE,
    cors: true
  }).listen(BACK_PORT, () => {
    console.log('\nAPI started on http://localhost%s', API_ROUTE);
  });
});
